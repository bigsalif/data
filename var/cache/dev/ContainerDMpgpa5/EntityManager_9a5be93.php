<?php

namespace ContainerDMpgpa5;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'src'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder83bfb = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerdec4a = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesdeb1b = [
        
    ];

    public function getConnection()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getConnection', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getMetadataFactory', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getExpressionBuilder', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'beginTransaction', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getCache', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getCache();
    }

    public function transactional($func)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'transactional', array('func' => $func), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'wrapInTransaction', array('func' => $func), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'commit', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->commit();
    }

    public function rollback()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'rollback', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getClassMetadata', array('className' => $className), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'createQuery', array('dql' => $dql), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'createNamedQuery', array('name' => $name), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'createQueryBuilder', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'flush', array('entity' => $entity), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'clear', array('entityName' => $entityName), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->clear($entityName);
    }

    public function close()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'close', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->close();
    }

    public function persist($entity)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'persist', array('entity' => $entity), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'remove', array('entity' => $entity), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'refresh', array('entity' => $entity), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'detach', array('entity' => $entity), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'merge', array('entity' => $entity), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getRepository', array('entityName' => $entityName), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'contains', array('entity' => $entity), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getEventManager', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getConfiguration', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'isOpen', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getUnitOfWork', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getProxyFactory', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'initializeObject', array('obj' => $obj), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'getFilters', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'isFiltersStateClean', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'hasFilters', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return $this->valueHolder83bfb->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerdec4a = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder83bfb) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder83bfb = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder83bfb->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, '__get', ['name' => $name], $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        if (isset(self::$publicPropertiesdeb1b[$name])) {
            return $this->valueHolder83bfb->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder83bfb;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder83bfb;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder83bfb;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder83bfb;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, '__isset', array('name' => $name), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder83bfb;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder83bfb;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, '__unset', array('name' => $name), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder83bfb;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder83bfb;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, '__clone', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        $this->valueHolder83bfb = clone $this->valueHolder83bfb;
    }

    public function __sleep()
    {
        $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, '__sleep', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;

        return array('valueHolder83bfb');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerdec4a = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerdec4a;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerdec4a && ($this->initializerdec4a->__invoke($valueHolder83bfb, $this, 'initializeProxy', array(), $this->initializerdec4a) || 1) && $this->valueHolder83bfb = $valueHolder83bfb;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder83bfb;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder83bfb;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
